Feature: meddental
This feature verifies the functionality on Med24h

@Verifycreateservice
Scenario: Login to CMS MEDDENTAL and create new service
Given I launch Chrome browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a service 

@Verifycreatenews
Scenario: Login to CMS MEDDENTAL and create new news
Given I launch Chrome browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a news

@Verifycreatenewcustomer
Scenario: Login to CMS MEDDENTAL and create new newscustomer
Given I launch Chrome browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a new customer

@Verifycreateserviceonfirefox
Scenario: Login to CMS MEDDENTAL and create new service on firefox
Given I launch Firefox browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a service 

@Verifycreatenewsonfirefox
Scenario: Login to CMS MEDDENTAL and create new news on firefox
Given I launch Firefox browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a news

@Verifycreatenewcustomeronfirefox
Scenario: Login to CMS MEDDENTAL and create new newscustomer on firefox
Given I launch Firefox browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I create a new customer

@Verifyimportimage
Scenario: Login to CMS MEDDENTAL and import a new image 
Given I launch Chrome browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I import a image to system

@VerifyimportimageFirefox
Scenario: Login to CMS MEDDENTAL and import a new image firefox
Given I launch Firefox browser
When I open 'http://meddental.imexsoft.vn/dashboard/Login.aspx'
And I login with username "admin" and password "anhtruong12"
And I import a image to system