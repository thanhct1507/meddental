package testRunners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="resources/features",tags= {"@VerifyimportimageFirefox,@Verifyimportimage,@Verifycreatenewsonfirefox,@Verifycreatenewcustomeronfirefox,@Verifycreateserviceonfirefox,@Verifycreateservice,@Verifycreatenews,@Verifycreatenewcustomer"},plugin= {"pretty","html:target/cucumber-reports"}, glue= {"Stepdefination"})
public class TestRunner {

}

//@VerifyimportimageFirefox,@Verifyimportimage,@Verifycreatenewsonfirefox,@Verifycreatenewcustomeronfirefox,@Verifycreateserviceonfirefox,@Verifycreateservice,@Verifycreatenews,@Verifycreatenewcustomer

