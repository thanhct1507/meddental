package Stepdefination;

import java.io.IOException;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import seleniumPages.ImportImage;
import seleniumPages.Login_and_create_services;
import seleniumPages.MEDDENTAL;
import seleniumPages.create_a_new_customer;
import seleniumPages.createnews;

public class Stepdef  {
	MEDDENTAL med = new MEDDENTAL();
	createnews news = new createnews();
	create_a_new_customer custo = new create_a_new_customer();
	Login_and_create_services service = new Login_and_create_services();
	ImportImage imp = new ImportImage();
	@Given("I launch Chrome browser")
	public void i_launch_Chrome_browser() {
	    med.launchBrowser();
	}
	@Given("I launch Firefox browser")
	public void i_launch_Firefox_browser() {
	    med.launchfirefoxBrowser();
	}
	@When("I open {string}")
	public void i_open_Med_h(String url) {
	   med.openURL(url);
	}

	@Then("I login with username {string} and password {string}")
	public void i_login_with_username_and_password(String username, String password) throws InterruptedException {
		service.i_login_with_usernameandpassword(username, password);
	}
	@Then("I create a service")
	public void i_create_a_service() throws InterruptedException {
		service.i_create_a_service();
	   
	}
	@Then("I create a news")
	public void i_create_a_news() throws InterruptedException {
		news.create_a_news();
	}
	@Then("I create a new customer")
	public void i_create_a_new_customer() throws InterruptedException {
	    custo.create_new_customer();
	}
	@Then("I import a image to system")
	public void I_import_a_image_to_system() throws Exception {
		imp.importimagetosystem();
	}
}
