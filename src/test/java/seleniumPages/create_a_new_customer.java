package seleniumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Page_BasePage;
import junit.framework.Assert;

import static org.testng.Assert.fail;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 

public class create_a_new_customer extends Page_BasePage {
	public void create_new_customer() throws InterruptedException {
		Actions buider = new Actions(driver);
		driver.findElement(By.partialLinkText("Quản lý")).click();
		driver.findElement(By.partialLinkText("Khách hàng")).click();
		driver.findElement(By.xpath("//*[contains(@class,'btn btn-default blue') and contains(@data-original-title,'Thêm mới')]")).click();
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[@id=\"IMEXsoftForm\"]/div/div[1]/h1")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(800);
		}
		driver.findElement(By.id("Name")).sendKeys("khách hàng test");
		driver.findElement(By.id("ServiceName_chosen")).click();
		driver.findElement(By.xpath("//*[contains(@class,'active-result') and contains(@data-option-array-index,'2')]")).click();
		driver.findElement(By.name("Comment")).sendKeys("Dịch vụ và các quy trình của nha khoa rất chuyên nghiệp, các bác sĩ tay nghề cao và vô cùng tận tình. Cám ơn MEDDENTAL!");
		driver.findElement(By.xpath("//*[contains(@class,'hidden-xs') and contains(text(),'Lưu & đóng')]")).click();
		for (int second = 0;; second++) {
			if (second >= 20) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[contains(@class,'btn waves-effect waves-light btn-danger btn-icon') and contains(@onclick,\"setcheck_delete('0');\")]")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(1000);
			
}
		driver.findElement(By.xpath("//*[contains(@data-original-title,'Xóa') and contains(@onclick,\"setcheck_delete('0');\")]")).click();
		for (int second = 0;; second++) {
			if (second >= 20) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[contains(@class,'swal2-confirm btn btn-success btn-rounded') and contains(text(),'Có, xóa luôn!')]")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(1000);
	}
		driver.findElement(By.xpath("//*[contains(@class,'swal2-confirm btn btn-success btn-rounded') and contains(text(),'Có, xóa luôn!')]")).click();
		driver.close();
	}
}
