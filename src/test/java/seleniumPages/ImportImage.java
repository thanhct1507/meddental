package seleniumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import common.Page_BasePage;

public class ImportImage extends Page_BasePage {

    
	public void importimagetosystem () throws Exception {
		// kiểm tra tên browser
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
	    String browserName = cap.getBrowserName().toLowerCase();
	    System.out.println(browserName);
	    //thực hiện click upload file
		driver.findElement(By.partialLinkText("Banner/Slide")).click();
		driver.findElement(By.partialLinkText("File tải lên")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//iframe[contains(@tabindex,'0') and contains(@allowtransparency,'true')]")));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@tabindex,'0') and contains(@allowtransparency,'true')]")));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@onclick,'void(0)') and contains(@role,'presentation')]"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@role,'treeitem') and contains(text(),'Home')]"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@id,'cke_9_label') and contains(text(),'Tải lên')]"))).click();
		//Chọn browser để thực thi autoit
		if (browserName.equals("firefox")) {
			Thread.sleep(2000);
			Runtime.getRuntime().exec(".\\driver\\Script.au4.exe");
		}
		else {
			Thread.sleep(2000);
			Runtime.getRuntime().exec(".\\driver\\Script.exe");
		}
		//xóa file vừa upload lên
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'design-and-build.jpg')]"))).click();
		builder.sendKeys(Keys.DELETE).build().perform();
		Thread.sleep(1500);
		builder.sendKeys(Keys.ENTER).build().perform();
		driver.switchTo().defaultContent();
		driver.close();
		
		
	}
	

	
}
