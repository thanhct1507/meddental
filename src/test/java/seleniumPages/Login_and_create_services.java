package seleniumPages;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;

import common.Page_BasePage;

public class Login_and_create_services extends Page_BasePage {
	public void i_login_with_usernameandpassword(String username, String password) throws InterruptedException {
		driver.findElement(By.name("UserName")).sendKeys(username);
		driver.findElement(By.name("Password")).sendKeys(password);
		driver.findElement(By.xpath("//*[contains(@class,'btn btn-info btn-md btn-block waves-effect text-center m-b-20') and contains(text(),'Đăng nhập')]")).click();
	}
		
	
//		WebElement element = driver.findElement(By.linkText("Managerhomepage.php"));
//		element.submit();
		/*for (int second = 0;; second++) {
		if (second >= 60) fail("timeout");
		try { if (driver.findElement(By.xpath("/html/body/div[3]/div/ul/li[1]/a")).isDisplayed()) break; } catch (Exception e) {}
		Thread.sleep(1000);
		}
		Boolean actual = driver.findElement(By.xpath("/html/body/div[3]/div/ul/li[1]/a")).isDisplayed();
		if (actual == true) {
			System.out.println("Pass");
		} else
		{
			System.out.println("Fail");
		}*/
		
	public void i_create_a_service() throws InterruptedException {
		// truy cập vào menu dịch vụ
		driver.findElement(By.partialLinkText("Quản lý")).click();
		driver.findElement(By.partialLinkText("Dịch vụ")).click();
		Thread.sleep(2000);
		//Thêm mới 1 dịch vụ
		driver.findElement(By.xpath("//*[contains(@class,'btn btn-default blue') and contains(@data-original-title,'Thêm mới')]")).click();
		//Đợi cho màn hình thêm mới dịch vụ hiển thị
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[@id=\"IMEXsoftForm\"]/div/div[1]/h1")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(1000);
}
		//Nhập các thông tin dịch vụ được thêm mới
		driver.findElement(By.id("Name")).sendKeys("Dịch vụ test");
		driver.findElement(By.xpath("//*[contains(@class,'chosen-container chosen-container-single form-control chosen') and contains(@id,'CategoryID_chosen')]")).click();
		driver.findElement(By.xpath("//*[contains(@class,'active-result') and contains(@data-option-array-index,'4')]")).click();
		for (int i=1; i<4; i++) {
		driver.findElement(By.xpath("//*[starts-with(@id,'name_')]")).sendKeys("Test"+i);
		driver.findElement(By.xpath("//*[starts-with(@id,'value_')]")).sendKeys(i+"00000");
		driver.findElement(By.xpath("//*[starts-with(@id,'unit_')]")).sendKeys(i+"R");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[contains(@class,'btn btn-default green') and contains(@data-original-title,'Thêm dịch vụ')]")).click();
}
		//Lưu và đóng màn hình thêm mới dịch vụ, kiểm tra nút xóa có hiển thị không
		driver.findElement(By.xpath("//*[contains(@class,'hidden-xs') and contains(text(),'Lưu & đóng')]")).click();
		for (int second = 0;; second++) {
			if (second >= 20) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[contains(@class,'btn waves-effect waves-light btn-danger btn-icon') and contains(@onclick,\"setcheck_delete('0');\")]")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(1000);
			
}
		// Tiến hành xóa dịch vụ vừa được thêm mới
		driver.findElement(By.xpath("//*[contains(@data-original-title,'Xóa') and contains(@onclick,\"setcheck_delete('0');\")]")).click();
		for (int second = 0;; second++) {
			if (second >= 20) fail("timeout");
			try { if (driver.findElement(By.xpath("//*[contains(@class,'swal2-confirm btn btn-success btn-rounded') and contains(text(),'Có, xóa luôn!')]")).isDisplayed()) break; } catch (Exception e) {}
			Thread.sleep(1000);
	}
		driver.findElement(By.xpath("//*[contains(@class,'swal2-confirm btn btn-success btn-rounded') and contains(text(),'Có, xóa luôn!')]")).click();
		driver.close();
}

}
/* for (int second = 0;; second++) {
if (second >= 60) fail("timeout");
try { if (driver.findElement(By.xpath("//*[contains(@class,'dropdown-item') and contains(@ng-reflect-router-link,'medlatec-question')]")).isDisplayed()) break; } catch (Exception e) {}
Thread.sleep(1000);
}*/
	

