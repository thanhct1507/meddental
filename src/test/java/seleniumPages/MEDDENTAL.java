package seleniumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Page_BasePage;

import junit.framework.Assert;

import static org.testng.Assert.fail;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
public class MEDDENTAL extends Page_BasePage {

	
	public void launchBrowser() {

        System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		jst = (JavascriptExecutor)driver;
		builder = new Actions(driver);
		
	}
	
	public void launchfirefoxBrowser() {

        System.setProperty("webdriver.chrome.driver", ".\\driver\\gekodriver.exe");
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);
		jst = (JavascriptExecutor)driver;
		builder = new Actions(driver);
	}
	public void openURL(String url) {
		driver.get(url);
		driver.manage().window().maximize();
	}
		
	

}
/* for (int second = 0;; second++) {
if (second >= 60) fail("timeout");
try { if (driver.findElement(By.xpath("//*[contains(@class,'dropdown-item') and contains(@ng-reflect-router-link,'medlatec-question')]")).isDisplayed()) break; } catch (Exception e) {}
Thread.sleep(1000);
}*/
	
	
	
	

